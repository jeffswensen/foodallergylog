using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace FoodAllergyLog.Android.Data {
    [Table("ScheduleEntry_Symptom")]
    public class ScheduleEntry_Symptom {
        [Column("ScheduleEntryId")]
        public int ScheduleEntryId { get; set; }
        [Column("SymptomId")]
        public int SymptomId { get; set; }
    }
}