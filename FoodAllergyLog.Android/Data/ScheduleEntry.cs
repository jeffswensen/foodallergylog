using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace FoodAllergyLog.Android.Data {
    [Table("ScheduleEntry")]
    public class ScheduleEntry {
        [PrimaryKey, AutoIncrement, Column("ScheduleEntryId")]
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int GeneralHealthRating { get; set; }
        public int ScheduleId { get; set; }
        [Ignore]
        public List<Symptom> Symptoms { get; set; }

        public ScheduleEntry() {
            Symptoms = new List<Symptom>();
        }

    }
}