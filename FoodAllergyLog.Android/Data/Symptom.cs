using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace FoodAllergyLog.Android.Data {
    [Table("Symptom")]
    public class Symptom {
        [PrimaryKey, AutoIncrement, Column("SymptomId")]
        public int Id { get; set; }
        public String Name { get; set; }
        
        public static string[] DefaultSymptomNames = new string[]{
                "Headache", "Gas", "Bloating", "Diarrhea", "Cramps", "Stomach Pain",
                "Heartburn", "Nausea", "Fatigue", "Muscle Pain", "Fever",
                "Loss of Appetite", "Vomiting"
            };
    }
}