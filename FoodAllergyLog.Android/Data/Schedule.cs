using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace FoodAllergyLog.Android.Data {
    [Table("Schedule")]
    public class Schedule {
        [PrimaryKey, AutoIncrement, Column("ScheduleId")]
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public String Description { get; set; }
        public int IntervalMinutes { get; set; }
        public int DurationMinutes { get; set; }
        public bool Completed { get; set; }

        public string ListTitle() {
            return String.Format("{0} - {1}", Description, Start.ToString());
        }
        [Ignore]
        public List<ScheduleEntry> Entries { get; set; }
        public Schedule() {
            Entries = new List<ScheduleEntry>();
            Completed = false;
        }
    }
}