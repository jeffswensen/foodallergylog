using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace FoodAllergyLog.Android.Data {
    public class ScheduleRepository {


        private string DatabaseName;
        private string DatabaseFilePath;
        private object lockObject;


        public ScheduleRepository() {
            DatabaseName = "ScheduleDatabase";
            DatabaseFilePath = BuildFilePath(DatabaseName);
            lockObject = new object();
        }
        
        private SQLiteConnection GetDatabase() {
            return new SQLiteConnection(DatabaseFilePath);
        }
        
        private string BuildFilePath(string databaseName) {
            return Path.Combine(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal),
                databaseName + ".db3");
        }

        public void InitializeDatabase() {
            using (var db = GetDatabase()) {
                CreateTables(db);
                AddDefaultSymptoms(db);
            }
        }

        private void AddDefaultSymptoms(SQLiteConnection db) {
            if (DefaultSymptomsAdded(db)) {
                return;
            }
            var symptom = new Symptom();
            for (var i = 0; i < Symptom.DefaultSymptomNames.Length; i++) {
                symptom.Name = Symptom.DefaultSymptomNames[i];
                lock (lockObject) {
                    db.Insert(symptom);
                }
            }
        }

        private bool DefaultSymptomsAdded(SQLiteConnection db) {
            return db.Table<Symptom>().Count() != 0;
        }

        private void CreateTables(SQLiteConnection db) {
            lock (lockObject) {
                db.CreateTable<Symptom>();
                db.CreateTable<Schedule>();
                db.CreateTable<ScheduleEntry>();
                db.CreateTable<ScheduleEntry_Symptom>();
            }
        }

        public IList<Symptom> AllSymptoms() {
            List<Symptom> allSymptoms;
            using (var db = GetDatabase()) {
                lock (lockObject) {
                    allSymptoms = db.Table<Symptom>().ToList();
                }
            }
            return allSymptoms;
        }

        public bool AddAdhocSchedule(int genHealthRating, int[] symptomIds) {
            var newSchedule = new Schedule();
            newSchedule.Description = "Auto Generated Log";
            newSchedule.Start = DateTime.UtcNow;
            newSchedule.IntervalMinutes = 0;
            newSchedule.DurationMinutes = 0;

            using (var db = GetDatabase()) {

                lock (lockObject) {
                    db.Insert(newSchedule);
                }

                var newScheduleEntry = new ScheduleEntry();
                newScheduleEntry.ScheduleId = newSchedule.Id;
                newScheduleEntry.Timestamp = newSchedule.Start;
                newScheduleEntry.GeneralHealthRating = genHealthRating;

                lock (lockObject) {
                    db.Insert(newScheduleEntry);
                }

                var newScheduleEntrySymptomsList = new List<ScheduleEntry_Symptom>();
                for (var i = 0; i < symptomIds.Length; i++) {
                    var newScheduleEntrySymptom = new ScheduleEntry_Symptom();
                    newScheduleEntrySymptom.ScheduleEntryId = newScheduleEntry.Id;
                    newScheduleEntrySymptom.SymptomId = symptomIds[i];
                    newScheduleEntrySymptomsList.Add(newScheduleEntrySymptom);
                }
                lock(lockObject) {
                    db.InsertAll(newScheduleEntrySymptomsList);
                }
            }
            return true;
        }

        public bool AddScheduleEntry(int scheduleId, int genHealthRating, int[] symptomIds) {
            var newScheduleEntry = new ScheduleEntry();
            newScheduleEntry.ScheduleId = scheduleId;
            newScheduleEntry.Timestamp = DateTime.UtcNow;
            newScheduleEntry.GeneralHealthRating = genHealthRating;

            using (var db = GetDatabase()) {
                lock (lockObject) {
                    db.Insert(newScheduleEntry);

                    var newScheduleEntrySymptomsList = new List<ScheduleEntry_Symptom>();
                    for (var i = 0; i < symptomIds.Length; i++) {
                        var newScheduleEntrySymptom = new ScheduleEntry_Symptom();
                        newScheduleEntrySymptom.ScheduleEntryId = newScheduleEntry.Id;
                        newScheduleEntrySymptom.SymptomId = symptomIds[i];
                        newScheduleEntrySymptomsList.Add(newScheduleEntrySymptom);
                    }
                    lock (lockObject) {
                        db.InsertAll(newScheduleEntrySymptomsList);
                    }
                }
            }
            return true;
        }

        public Schedule AddNewSchedule(string Description, int intervalMinutes, int durationMinutes) {
            var newSchedule = new Schedule();
            newSchedule.Start = DateTime.UtcNow;
            newSchedule.IntervalMinutes = intervalMinutes;
            newSchedule.DurationMinutes = durationMinutes;

            using (var db = GetDatabase()) {
                lock (lockObject) {
                    db.Insert(newSchedule);
                }
            }
            return newSchedule;
        }

        public List<Schedule> AllSchedules() {
            using (var db = GetDatabase()) {
                return db.Table<Schedule>().ToList();
            }
        }

        public Schedule FullSchedule(int scheduleId) {
            Schedule schedule;
            using (var db = GetDatabase()) {
                lock (lockObject) {
                    schedule = db.Get<Schedule>(scheduleId);
                }
            }

            schedule.Entries = ScheduleEntriesWithSymptoms(schedule.Id);
            return schedule;
        }

        public List<ScheduleEntry> ScheduleEntriesWithSymptoms(int scheduleId) {
            List<ScheduleEntry> entries;
            using (var db = GetDatabase()) {
                lock (lockObject) {
                    entries = (from e in db.Table<ScheduleEntry>()
                              where e.ScheduleId.Equals(scheduleId)
                              orderby e.Timestamp
                              select e).ToList();
                }
            }

            foreach (var entry in entries) {
                entry.Symptoms = Symptoms(entry.Id);
            }
            return entries;
        }

        public List<Symptom> Symptoms(int scheduleEntryId) {
            List<Symptom> symptoms;
            using (var db = GetDatabase()) {
                List<int> symptomIds;
                lock (lockObject) {
                    symptomIds = (from s in db.Table<ScheduleEntry_Symptom>()
                                 where s.ScheduleEntryId == scheduleEntryId
                                 select s).Select(se => se.SymptomId).ToList();
                }
                
                lock (lockObject) {
                    symptoms = (from s in db.Table<Symptom>()
                                where symptomIds.Contains(s.Id)
                                select s).ToList();
                }
            }
            return symptoms;
        }

        public string ScheduleListTitle(int scheduleId) {
            var sb = new StringBuilder();
            Schedule schedule;
            using (var db = GetDatabase()) {
                lock (lockObject) {
                    schedule = db.Get<Schedule>(scheduleId);
                }
            }
            sb.AppendFormat("{0} @ {1}", schedule.Description, schedule.Start.ToString());
            return sb.ToString();
        }
    }
}