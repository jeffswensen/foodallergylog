﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using FoodAllergyLog.Android.Data;

namespace FoodAllergyLog.Android {
    [Activity(Label = "Food Allergy Log", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity {

        Button startNewScheduleBtn;
        Button recordCurrentSymptomsBtn;
        Button viewPastSchedulesBtn;

        ScheduleRepository scheduleRepository;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            scheduleRepository = new ScheduleRepository();
            scheduleRepository.InitializeDatabase();            

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            startNewScheduleBtn = FindViewById<Button>(Resource.Id.buttonNewSchedule);
            recordCurrentSymptomsBtn = FindViewById<Button>(Resource.Id.buttonRecordCurrentSymptoms);
            viewPastSchedulesBtn = FindViewById<Button>(Resource.Id.buttonViewPastSchedules);

            startNewScheduleBtn.Click += startNewScheduleBtn_Click;
            recordCurrentSymptomsBtn.Click += recordCurrentSymptomsBtn_Click;
            viewPastSchedulesBtn.Click += viewPastSchedulesBtn_Click;

        }

        void startNewScheduleBtn_Click(object sender, EventArgs e) {
            var newScheduleIntent = new Intent(this, typeof(NewScheduleActivity));
            StartActivity(newScheduleIntent);
        }

        void recordCurrentSymptomsBtn_Click(object sender, EventArgs e) {
            var symptomsPromptIntent = new Intent(this, typeof(PromptActivity));
            StartActivity(symptomsPromptIntent);
        }
        void viewPastSchedulesBtn_Click(object sender, EventArgs e) {
            var pastSchedulesIntent = new Intent(this, typeof(SchedulesMainActivity));
            StartActivity(pastSchedulesIntent);
        }


    }
}

