using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using FoodAllergyLog.Android.Data;
using Android.Provider;
using Android.Util;
using Android.Net;
using Java.Util;

namespace FoodAllergyLog.Android {
    [Activity(Label = "NewScheduleActivity")]
    public class NewScheduleActivity : Activity {

        EditText intervalTimeText;
        EditText totalTimeText;
        EditText descriptionText;
        Button beginScheduleBtn;
        Button cancelBtn;
        Spinner intervalUnitSpinner;
        Spinner totalTimeUnitSpinner;

        ScheduleRepository scheduleRepository;
                
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            scheduleRepository = new ScheduleRepository();

            // Create your application here
            SetContentView(Resource.Layout.NewSchedule);

            intervalTimeText = FindViewById<EditText>(Resource.Id.editTextIntervalMinutes);
            totalTimeText = FindViewById<EditText>(Resource.Id.editTextTotalTime);
            descriptionText = FindViewById<EditText>(Resource.Id.editTextFoodConsumed);
            beginScheduleBtn = FindViewById<Button>(Resource.Id.buttonBeginSchedule);
            cancelBtn = FindViewById<Button>(Resource.Id.buttonNewScheduleCancel);
            intervalUnitSpinner = FindViewById<Spinner>(Resource.Id.intervalUnitSpinner);
            totalTimeUnitSpinner = FindViewById<Spinner>(Resource.Id.totalTimeUnitSpinner);

            beginScheduleBtn.Click += beginScheduleBtn_Click;
            cancelBtn.Click += cancelBtn_Click;
        }

        void beginScheduleBtn_Click(object sender, EventArgs e) {

            if (String.IsNullOrEmpty(intervalTimeText.Text)) {
                Toast.MakeText(this, "Please Enter an Interval", ToastLength.Long).Show();
                return;
            }

            if (String.IsNullOrEmpty(totalTimeText.Text)) {
                Toast.MakeText(this, "Please Enter a Duration", ToastLength.Long).Show();
                return;
            }

            var intervalMultiplier = 1;
            if ("hours".Equals((string)intervalUnitSpinner.SelectedItem)) {
                intervalMultiplier = 60;
            }
            var intervalMinutes = Int32.Parse(intervalTimeText.Text) * intervalMultiplier;

            var totalTimeMultiplier = 1;
            if ("hours".Equals((string)totalTimeUnitSpinner.SelectedItem)) {
                totalTimeMultiplier = 60;
            }
            var totalTimeMinutes = Int32.Parse(totalTimeText.Text) * totalTimeMultiplier;

            var newSchedule = scheduleRepository.AddNewSchedule(
                descriptionText.Text,
                intervalMinutes,
                totalTimeMinutes);

            long calendarId = findCalendarId("FoodAllergyLog");
            if (calendarId == -1) {
                calendarId = createCalendar("FoodAllergyLog");
            }

            Log.Debug("NewScheduleActivity", "Created/Found FoodAllergyLog ID:{0}", calendarId);

            addCalendarEvents(newSchedule, calendarId);

            Toast.MakeText(this, "Event Reminders Created", ToastLength.Long).Show();
            Finish();
        }

        private void addCalendarEvents(Schedule schedule, long calendarId) {
            int totalMinutes = schedule.DurationMinutes;
            int eventCount = (int)totalMinutes / schedule.IntervalMinutes;

            var symptomEntryLink = "Click Here To Record Your Symptoms: "
                + "(Choose to open link on device) www.foodallergylog.com/schedule/" 
                + schedule.Id;

            for (var i = 0; i < eventCount; i++) {
                ContentValues eventValues = new ContentValues();
                var startDateTime = DateTime.SpecifyKind(schedule.Start.AddMinutes((i+1) * schedule.IntervalMinutes), DateTimeKind.Utc);
                var endDateTime = DateTime.SpecifyKind(schedule.Start.AddMinutes(((i+1) * schedule.IntervalMinutes) + 5), DateTimeKind.Utc);
                Log.Debug("NewScheduleActivity", "startDateTime: {0} endDateTime: {1}", startDateTime.ToString(), endDateTime.ToString());
                eventValues.Put(CalendarContract.Events.InterfaceConsts.CalendarId, calendarId);
                eventValues.Put(CalendarContract.Events.InterfaceConsts.Title, "Food Allergy Log Reminder");
                eventValues.Put(CalendarContract.Events.InterfaceConsts.Description,
                    symptomEntryLink);
                eventValues.Put(CalendarContract.Events.InterfaceConsts.Dtstart,
                    GetDateTimeMS(startDateTime));
                eventValues.Put(CalendarContract.Events.InterfaceConsts.Dtend,
                    GetDateTimeMS(endDateTime));
                eventValues.Put(CalendarContract.Events.InterfaceConsts.EventTimezone, "UTC");
                eventValues.Put(CalendarContract.Events.InterfaceConsts.EventEndTimezone, "UTC");

                var eventUri = ContentResolver.Insert(CalendarContract.Events.ContentUri,
                    eventValues);

                long eventId = long.Parse(eventUri.LastPathSegment);

                ContentValues reminderValues = new ContentValues();
                ContentValues remindervalues = new ContentValues();
                remindervalues.Put(CalendarContract.Reminders.InterfaceConsts.Minutes, 1);
                remindervalues.Put(CalendarContract.Reminders.InterfaceConsts.EventId, eventId);
                remindervalues.Put(CalendarContract.Reminders.InterfaceConsts.Method, (int)global::Android.Provider.RemindersMethod.Alert);
                var reminderURI = ContentResolver.Insert(CalendarContract.Reminders.ContentUri, remindervalues);
            }
        }

        long GetDateTimeMS(DateTime dt) {
            return GetDateTimeMS(
                yr: dt.Year,
                month: dt.Month - 1,
                day: dt.Day,
                hr: dt.Hour,
                min: dt.Minute
                );
        }

        long GetDateTimeMS(int yr, int month, int day, int hr, int min) {
            Calendar c = Calendar.GetInstance(Java.Util.TimeZone.Default);

            c.Set(Java.Util.CalendarField.DayOfMonth, day);
            c.Set(Java.Util.CalendarField.HourOfDay, hr);
            c.Set(Java.Util.CalendarField.Minute, min);
            c.Set(Java.Util.CalendarField.Month, month);
            c.Set(Java.Util.CalendarField.Year, yr);

            return c.TimeInMillis;
        }

        private long createCalendar(string calendarName) {
            ContentValues cv = buildContentValues(calendarName);
            global::Android.Net.Uri calUri = buildCalUri();
            ContentResolver.Insert(calUri, cv);

            return findCalendarId(calendarName);
        }

        private long findCalendarId(string calendarName) {
            var calendarsUri = CalendarContract.Calendars.ContentUri;

            string[] calendarsProjection = {
                                               CalendarContract.Calendars.InterfaceConsts.Id,
                                               CalendarContract.Calendars.InterfaceConsts.CalendarDisplayName,
                                               CalendarContract.Calendars.InterfaceConsts.AccountName
                                           };

            var cursor = ManagedQuery(calendarsUri, calendarsProjection, null, null, null);
            long falCalendarId = -1;
            while (cursor.MoveToNext()) {
                long calId;
                string calDisplayName;
                string calAccountName;

                calId = cursor.GetLong(0);
                calDisplayName = cursor.GetString(1);
                calAccountName = cursor.GetString(2);

                if (calDisplayName == calendarName) {
                    falCalendarId = calId;
                }

            }
            return falCalendarId;
        }

        private ContentValues buildContentValues(string calName) {
            ContentValues cv = new ContentValues();
            cv.Put(CalendarContract.Calendars.InterfaceConsts.AccountName, "private");
            cv.Put(CalendarContract.Calendars.InterfaceConsts.AccountType, CalendarContract.AccountTypeLocal);
            cv.Put(CalendarContract.Calendars.InterfaceConsts.CalendarDisplayName, calName);
            cv.Put(CalendarContract.Calendars.InterfaceConsts.OwnerAccount, "private");
            cv.Put(CalendarContract.Calendars.InterfaceConsts.CalendarAccessLevel, 700); // CAL_ACCESS_OWNER
            return cv;
        }

        private global::Android.Net.Uri buildCalUri() {
            return CalendarContract.Calendars.ContentUri
                .BuildUpon()
                .AppendQueryParameter(CalendarContract.CallerIsSyncadapter, "true")
                .AppendQueryParameter(CalendarContract.Calendars.InterfaceConsts.AccountName, "private")
                .AppendQueryParameter(CalendarContract.Calendars.InterfaceConsts.AccountType, CalendarContract.AccountTypeLocal)
                .Build();
        }

        void cancelBtn_Click(object sender, EventArgs e) {
            Finish();
        }
    }
}