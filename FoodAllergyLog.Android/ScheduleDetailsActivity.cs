using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FoodAllergyLog.Android {
    [Activity(Label = "ScheduleDetailsActivity")]
    public class ScheduleDetailsActivity : Activity {
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            var scheduleId = Intent.Extras.GetInt("current_schedule_id", 0);

            var details = ScheduleDetailsFragment.NewInstance(scheduleId); // DetailsFragment.NewInstance is a factory method to create a Details Fragment
            var fragmentTransaction = FragmentManager.BeginTransaction();
            fragmentTransaction.Add(global::Android.Resource.Id.Content, details);
            fragmentTransaction.Commit();
        }
    }
}