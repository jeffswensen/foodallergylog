using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using FoodAllergyLog.Android.Data;

namespace FoodAllergyLog.Android {
    public class ScheduleDetailsFragment : Fragment {

        private ScheduleRepository scheduleRepository;
        public static ScheduleDetailsFragment NewInstance(int scheduleId) {
            var detailsFrag = new ScheduleDetailsFragment { Arguments = new Bundle() };
            detailsFrag.Arguments.PutInt("current_schedule_id", scheduleId);
            return detailsFrag;
        }
        public int ShownScheduleId {
            get { return Arguments.GetInt("current_schedule_id", 0); }
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            if (container == null) {
                // Currently in a layout without a container, so no reason to create our view.
                return null;
            }
            scheduleRepository = new ScheduleRepository();
            var fullSchedule = scheduleRepository.FullSchedule(ShownScheduleId);

            var linearRoot = new LinearLayout(Activity);
            var lpRoot = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FillParent,
                ViewGroup.LayoutParams.FillParent);
            linearRoot.Orientation = Orientation.Vertical;
            linearRoot.LayoutParameters = lpRoot;
            linearRoot.ShowDividers = ShowDividers.Middle;
            
            // description & start
            var startText = new TextView(Activity);
            startText.TextSize = 12;
            startText.Gravity = GravityFlags.Right;
            StringBuilder sb = new StringBuilder();
            sb.Append("Started @ ");
            sb.Append(fullSchedule.Start.ToLocalTime().ToShortDateString());
            sb.Append(" - ");
            sb.Append(fullSchedule.Start.ToLocalTime().ToShortTimeString());
            startText.Text = sb.ToString();
            linearRoot.AddView(startText);

            var descText = new TextView(Activity);
            descText.TextSize = 16;
            descText.Gravity = GravityFlags.Left;
            descText.Text = fullSchedule.Description;
            linearRoot.AddView(descText);

            // symptoms
            //var scroller = new ScrollView(Activity);
            //var text = new TextView(Activity);
            //var padding = Convert.ToInt32(TypedValue.ApplyDimension(ComplexUnitType.Dip, 4, Activity.Resources.DisplayMetrics));
            //text.SetPadding(padding, padding, padding, padding);
            //text.TextSize = 24;
            //text.Text = scheduleRepository.ScheduleListTitle(ShownScheduleId);
            //scroller.AddView(text);
            //linearRoot.AddView(scroller);


            return linearRoot;
        }
    }
}