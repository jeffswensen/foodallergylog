using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using FoodAllergyLog.Android.Data;

namespace FoodAllergyLog.Android {
    [Activity(Label = "Food Allergy Log")]
    [IntentFilter(new[] { Intent.ActionView }, DataScheme = "http", DataHost = "www.foodallergylog.com", Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable, Intent.CategoryLauncher }, DataPathPrefix="/schedule")]
    public class PromptActivity : Activity {

        RatingBar genHealthBar;
        Button submitButton;
        Button cancelButton;
        LinearLayout symptomParentLayout;

        ScheduleRepository scheduleRepository;
        Dictionary<int, CheckBox> symptomCheckBoxes;

        int CurrentScheduleId;
        
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            scheduleRepository = new ScheduleRepository();
            
            // Create your application here
            SetContentView(Resource.Layout.Prompt);

            genHealthBar = FindViewById<RatingBar>(Resource.Id.ratingBarGenHealth);
            genHealthBar.NumStars = 5;
            genHealthBar.StepSize = 1.0f;
            submitButton = FindViewById<Button>(Resource.Id.buttonSymSubmit);
            cancelButton = FindViewById<Button>(Resource.Id.buttonSymCancel);
            symptomParentLayout = FindViewById<LinearLayout>(Resource.Id.symptomParentLayout);

            submitButton.Click += submitButton_Click;
            cancelButton.Click += cancelButton_Click;

            symptomCheckBoxes = new Dictionary<int, CheckBox>();

            addSymptomsToUi();

            getScheduleIdFromIntent();
        }

        void getScheduleIdFromIntent() {
            global::Android.Net.Uri data = Intent.Data;
            if (data != null) {
                string scheme = data.Scheme;
                string objectType = data.Host;
                var pathParts = data.PathSegments;
                string scheduleId = pathParts.Skip(1).First();
                CurrentScheduleId = Int32.Parse(scheduleId);
            }
        }

        void addSymptomsToUi() {
            foreach (var symptom in scheduleRepository.AllSymptoms()) {
                CheckBox checkBox = new CheckBox(this);
                checkBox.LayoutParameters = symptomParentLayout.LayoutParameters;
                checkBox.Text = symptom.Name;
                symptomParentLayout.AddView(checkBox);
                symptomCheckBoxes.Add(symptom.Id, checkBox);
            }
        }

        void cancelButton_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        void submitButton_Click(object sender, EventArgs e) {
            var symptomIds = symptomCheckBoxes.Where(s => s.Value.Checked).Select(entry => entry.Key).ToArray();
            var genHealthRating = (int)genHealthBar.Rating;
            if (genHealthRating == 0) {
                Toast.MakeText(this, "Please Choose 1-5 Stars", ToastLength.Long).Show();
                return;
            }
            if (CurrentScheduleId == null) {
                if (scheduleRepository.AddAdhocSchedule(genHealthRating, symptomIds)) {
                    Toast.MakeText(this, "Entry Saved", ToastLength.Long).Show();
                    Finish();
                } else {
                    Toast.MakeText(this, "Save Failed", ToastLength.Long).Show();
                }
            } else {
                // link from calendar
                if (scheduleRepository.AddScheduleEntry(CurrentScheduleId, genHealthRating, symptomIds)) {
                    Toast.MakeText(this, "Entry Saved", ToastLength.Long).Show();
                    Finish();
                } else {
                    Toast.MakeText(this, "Save Failed", ToastLength.Long).Show();
                }
            }
        }

    }
}