using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using FoodAllergyLog.Android.Data;

namespace FoodAllergyLog.Android {
    public class SchedulesFragment : ListFragment {

        private bool _isDualPane;
        private int _currentScheduleId;
        private ScheduleRepository scheduleRepository;
        private Schedule[] allSchedules;
        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            scheduleRepository = new ScheduleRepository();
            allSchedules = scheduleRepository.AllSchedules().ToArray();
        }
        
        public override void OnActivityCreated(Bundle savedInstanceState) {
            base.OnActivityCreated(savedInstanceState);
            string[] scheduleTitles = allSchedules.Select(s => s.ListTitle()).ToArray();
            var adapter = new ArrayAdapter<String>(Activity, global::Android.Resource.Layout.SimpleListItemChecked, scheduleTitles);
            ListAdapter = adapter;
            if (savedInstanceState != null) {
                _currentScheduleId = savedInstanceState.GetInt("current_schedule_id", 0);
            }
            var detailsFrame = Activity.FindViewById<View>(Resource.Id.details);
            _isDualPane = detailsFrame != null && detailsFrame.Visibility == ViewStates.Visible;
            if (_isDualPane) {
                ListView.ChoiceMode = ChoiceMode.Single;
                ShowDetails(_currentScheduleId);
            }
        }

        public override void OnListItemClick(ListView l, View v, int position, long id) {
            ShowDetails(position);
        }

        private void ShowDetails(int arrayIndex) {
            _currentScheduleId = allSchedules[arrayIndex].Id;
            if (_isDualPane) {
                // We can display everything in place with fragments.
                // Have the list highlight this item and show the data.
                ListView.SetItemChecked(arrayIndex, true);
                // Check what fragment is shown, replace if needed.
                var details = FragmentManager.FindFragmentById(Resource.Id.details) as ScheduleDetailsFragment;
                if (details == null || details.ShownScheduleId != _currentScheduleId) {
                    // Make new fragment to show this selection.
                    details = ScheduleDetailsFragment.NewInstance(_currentScheduleId);
                    // Execute a transaction, replacing any existing
                    // fragment with this one inside the frame.
                    var ft = FragmentManager.BeginTransaction();
                    ft.Replace(Resource.Id.details, details);
                    ft.SetTransition(FragmentTransit.FragmentFade);
                    ft.Commit();
                }
            } else {
                // Otherwise we need to launch a new Activity to display
                // the dialog fragment with selected text.
                var intent = new Intent();
                intent.SetClass(Activity, typeof(ScheduleDetailsActivity));
                intent.PutExtra("current_schedule_id", _currentScheduleId);
                StartActivity(intent);
            }
        }
    }
}